# Lightweight Time Series Database

TSDB is an embeddable lightweight time series database using [Badger](https://github.com/dgraph-io/badger) as backend

## Getting started

### Installing

Download and install it:

```sh
$ go get gitlab.com/gohome1/tsdb
```

### Usage

#### Opening a database

```go
package main

import (
    "log"
    "gtlab.com/gohome1/tsdb"
)

func main() {
    tsdb, err := NewTsdb("test.db")
    if err != nil {
        log.Fatal(err)
    }
    defer tsdb.Close()
    // Your code here…
}
```

#### Writing data

```go
package main

import (
    "log"
    "gtlab.com/gohome1/tsdb"
)

func main() {
    // open database first
    data := Data{time: time.Now().UnixNano(), value: `{"temperature": 22}`}
    err := tsdb.Write("kitchen/temperature", data)

    if err != nil {
        log.Print("Write was not successful")
    }
}
```

#### Reading data