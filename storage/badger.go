package storage

import (
	"github.com/dgraph-io/badger/v2"
	"strconv"
	"strings"
)

type BadgerStorage struct {
	db *badger.DB
}

const KeyTimeDelimiter = ":"

func (s *BadgerStorage) Open(file string) error {
	db, err := badger.Open(badger.DefaultOptions(file))
	s.db = db
	return err
}

func (s *BadgerStorage) Close() error {
	return s.db.Close()
}

func (s *BadgerStorage) Write(key string, time int64, value string) error {
	err := s.db.Update(func(tx *badger.Txn) error {
		key = key + KeyTimeDelimiter + strconv.FormatInt(time, 10)
		err := tx.Set([]byte(key), []byte(value))
		return err
	})

	return err
}

func (s *BadgerStorage) Query(key string, offset int, limit int) ([]*KeyValue, error) {
	result := make([]*KeyValue, 0)

	err := s.db.View(func(txn *badger.Txn) error {
		it := txn.NewIterator(badger.DefaultIteratorOptions)
		defer it.Close()
		prefix := []byte(key)

		for it.Seek(prefix); it.ValidForPrefix(prefix); it.Next() {
			if offset > 0 {
				offset--
				continue
			}

			item := it.Item()
			k := string(item.Key()[:])

			err := item.Value(func(v []byte) error {
				nanoTime := strings.TrimPrefix(k, key+KeyTimeDelimiter)
				result = append(result, &KeyValue{Key: nanoTime, Value: string(v[:])})
				return nil
			})

			if err != nil {
				return err
			}

			if limit > 0 {
				limit--
				if limit == 0 {
					break
				}
			}
		}
		return nil
	})

	return result, err
}
