package storage

type KeyValue struct {
	Key   string
	Value string
}

type Storage interface {
	Open(file string) error
	Close() error
	Write(key string, time int64, value string) error
	Query(key string, offset int, limit int) ([]*KeyValue, error)
}

func NewBadgerStorage(file string) (Storage, error) {
	s := &BadgerStorage{}
	err := s.Open(file)
	return s, err
}
